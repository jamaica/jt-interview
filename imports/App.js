import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from "./pages/Home";
import View from "./pages/View";

const App = () => {
    const config = [{
        control: 'input',
        type: 'text',
        label: 'Company Name',
        name: 'company_name',
    }, {
        control: 'input',
        type: 'text',
        label: 'Position Title',
        name: 'position_title',
    }, {
        control: 'textarea',
        type: 'textarea',
        label: 'Position Description',
        name: 'position_descriptions',
    }];

    return (
        <Switch>
            <Route
                exact
                path="/"
                render={() => <Home config={config}/>}
            />
            <Route
                exact
                path="/view"
                render={() => <View config={config}/>}
            />
        </Switch>
    );
};

export default App;
