import styled from 'styled-components';
import React, { Component } from 'react';
import Spinner from "react-md-spinner";
import { JobRepository } from './api';
import { JTTextarea, JTInput, StyledButton } from './controls';
import { Meteor } from "meteor/meteor";

const StyledForm = styled.form`
  font: 95% Arial, Helvetica, sans-serif;
  max-width: 400px;
  margin: 10px auto;
  padding: 16px; 
  background: #F7F7F7;
`;

/* TODO Weird behaviour does not allow custom elements in createElement EvenInPascalCase. */
const map = {
    input: JTInput,
    textarea: JTTextarea,
};

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            submitInProgress: false,
        };
        this.config = props.config;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            data: {
                ...this.state.data,
                [event.target.name]: event.target.value
            }
        });
    }

    // TODO move up
    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitInProgress: true });
        JobRepository.insert(
            this.state.data,
            () => this.setState({ submitInProgress: false })
        );

        Meteor.isClient && Meteor.call('postToSlack', this.state.data);
    }

    render() {
        const children = this.config.map((e, key) => {
            const { control, ...options } = e;

            return React.createElement(map[control], {
                key, onChange: this.handleChange, ...options
            });
        });

        const JTSpinner = styled(Spinner)`
            position:fixed;top:50%;left:50%
        `;
        return (
            <StyledForm onSubmit={this.handleSubmit}>
                {children}
                {this.state.submitInProgress ?
                    <JTSpinner size={20}/> :
                    <StyledButton>Submit</StyledButton>
                }
            </StyledForm>
        );
    }
}

export default Form;
