import { Meteor } from 'meteor/meteor';
import React, { Fragment } from "react";
import { Header } from '../components/Header';
import { withTracker } from "meteor/react-meteor-data";
import { JobRepository } from "../api";
import styled from "styled-components";

const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
`;

const Elem = styled.div`
    flex-item: 1;
    width: 33%;
    word-wrap: break-word;
`;

export const View = ({ data }) => {
    const elements = data.map((job, index) => {
            const { position_title, position_descriptions, company_name } = job;

            return (
                <Fragment key={index}>
                    <Elem>{position_title}</Elem>
                    <Elem>{position_descriptions}</Elem>
                    <Elem>{company_name}</Elem>
                </Fragment>
            );
        }
    );

    return (
        <Fragment>
            <Header>Job details</Header>
            <Wrapper>
                <Elem>Position Title</Elem>
                <Elem>Position Descriptions</Elem>
                <Elem>Company Name</Elem>
                {elements}
            </Wrapper>
        </Fragment>
    );
};

export default withTracker(() => {
    Meteor.isClient && Meteor.subscribe('descriptions');

    return {
        data: JobRepository.find({}).fetch(),
    };
})(View);
