import React, { Fragment } from "react";
import Spinner from "react-md-spinner";
import Form from '../Form';
import { Header } from '../components/Header';

const Home = ({config}) => {
    return (config.length) ?
        <Fragment>
            <Header>Job details</Header>
            <Form config={config} />
        </Fragment> :
        <Spinner/>;
};

export default Home;
