import { Mongo } from 'meteor/mongo';

export const JobRepository = new Mongo.Collection('job_description');
