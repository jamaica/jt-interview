import React from 'react';
import styled from 'styled-components';

const controlStyle = `
    -webkit-transition: all 0.30s ease-in-out;
	-moz-transition: all 0.30s ease-in-out;
	-ms-transition: all 0.30s ease-in-out;
	-o-transition: all 0.30s ease-in-out;
	outline: none;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	width: 100%;
	background: #fff;
	margin-bottom: 4%;
	border: 1px solid #ccc;
	padding: 3%;
	color: #555;
	font: 95% Arial, Helvetica, sans-serif;
	&:focus {
	 box-shadow: 0 0 5px #43D1AF;
	 padding: 3%;
	 border: 1px solid #43D1AF;
    }
`;
export const StyledInput = styled.input`${controlStyle}`;
export const StyledTextarea = styled.textarea`${controlStyle}`;


export const StyledButton = styled.button`
    box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	width: 100%;
	padding: 3%;
	background: #43D1AF;
	border-bottom: 2px solid #30C29E;
	border-top-style: none;
	border-right-style: none;
	border-left-style: none;	
	color: #fff;
	&:hover {
        background: #2EBC99;
	}
`;


export const JTInput = ({ label, value, ...rest }) =>
    <StyledInput placeholder={label} {...rest}>{value}</StyledInput>;

export const JTTextarea = ({ label, value, ...rest }) =>
    <StyledTextarea placeholder={label} {...rest}>{value}</StyledTextarea>;
