import styled from "styled-components";

export const Header = styled.h2`
    background: #43D1AF;
	padding: 5px 0;
	font-size: 140%;
	font-weight: 300;
	text-align: center;
	color: #fff;
	margin: -4px -4px 4px -4px;
`;
