import React from 'react';
import { Meteor } from 'meteor/meteor';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import App from '../imports/App.js';

Meteor.startup(() => {
    ReactDOM.hydrate(
        <BrowserRouter>
            <App/>
        </BrowserRouter>,
        document.getElementById('app')
    );
});
