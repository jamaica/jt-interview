import React from 'react';
import { Meteor } from 'meteor/meteor';
import '../imports/api';
import { onPageLoad } from 'meteor/server-render';
import { renderToString } from 'react-dom/server';
import { ServerStyleSheet } from 'styled-components'
import { StaticRouter } from 'react-router-dom';
import App from '../imports/App';
import { JobRepository } from '../imports/api';

Meteor.publish('descriptions', () => JobRepository.find({}));

Meteor.methods({
    postToSlack(data) {
        try {
            const payload = Object.values(data);
            if (!payload.length) {
                return true;
            }
            
            HTTP.call(
                'POST',
                Meteor.settings.slack.url, {
                    data: { text: payload.join(', ') },
                });

            return true;
        } catch (e) {
            return false;
        }
    }
});

onPageLoad((sink) => {
    const sheet = new ServerStyleSheet();
    const context = {};

    const html = renderToString(
        sheet.collectStyles(
            <StaticRouter location={sink.request.url} context={context}>
                <App/>
            </StaticRouter>
        )
    );

    sink.renderIntoElementById('app', html);
    sink.appendToHead(sheet.getStyleTags());
});
